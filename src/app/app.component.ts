import { Component } from '@angular/core';
import { Quizmodel } from './models/quiz.model';
import { Answermodel } from './models/answer.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  myarray = [];
  i = 0;
  identitys = ['Nam', 'Quang Anh'];
  newstr: string;

  quizList: Quizmodel[] = [
    {
      ID: 1, identity: 'Nam', question: 'The Simpsons is primarily set in which fictional US town?',
      anslistobj: ['Quahog', 'Shelbyville', 'Smallville', 'Springfield'],
      answer: 'Springfield'
    },
    {
      ID: 2, identity: 'Nam', question: 'The Simpsons first appeared as shorts during which US variety TV show?',
      anslistobj: ['In Living Color', 'The Muppet Show', 'Saturday Night Live', 'The Tracey Ullman Show'],
      answer: 'The Tracey Ullman Show'
    },
    {
      ID: 3, identity: 'Quang Anh', question: 'In what year did the Simpsons shorts debut on the Tracey Ullman Show?',
      anslistobj: ['1981', '1984', '1987', '1990'],
      answer: '1987'
    },
    {
      ID: 4, identity: 'Quang Anh', question: 'Who created The Simpsons?',
      anslistobj: ['Danny Antonucci', 'Matt Groening', 'Mike Judge', 'Seth MacFarlane'],
      answer: 'Matt Groening'
    },
    {
      ID: 5, identity: 'Nam', question: 'Which instrument is Lisa Simpson best known to play?',
      anslistobj: ['Clarinet', 'Oboe', 'Piccolo', 'Saxophone'],
      answer: 'Saxophone'
    },
    {
      ID: 6, identity: 'Quang Anh', question: 'Who wrote the theme music to The Simpsons?',
      anslistobj: ['Danny Elfman', 'Henry Mancini', 'Mike Post', 'Mark Snow'],
      answer: 'Danny Elfman'
    },
  ];

  quizLength: number;
  selectedIdentity: Quizmodel[] = [];
  question: string;
  selectedValue: string;
  option: any[];
  selectedIdentitys: any[];
  answerkey: AnswerKey[] = [];
  marks = 0;

  gettingIdentity() {
    this.selectedIdentitys = this.quizList.filter(i => (i.identity === this.selectedValue));
    this.question = this.selectedIdentitys[0].question;
    this.option = this.selectedIdentitys[0].anslistobj;
    this.i = 0;
    this.quizLength = this.selectedIdentitys.length;
  }

  next() {
     ++ this.i;
     this.question = this.selectedIdentitys[this.i].question;
     this.option = this.selectedIdentitys[this.i].anslistobj;
  }

  previous() {
    -- this.i;
    this.question = this.selectedIdentitys[this.i].question;
    this.option = this.selectedIdentitys[this.i].anslistobj;
 }

 check(e: { target: { check: any; }; }, str: string, answer: string) {
   if (e.target.check) {
     this.answerkey.push(new AnswerKey(str, answer));
   } else {
     this.answerkey.splice(0, 1);
   }
   console.log(this.answerkey);
   this.recursivecheck();
 }

 generatemark() {
   for (let i = 0; i < this.answerkey.length; i++) {
     if  (this.answerkey[i].choosen == this.quizList[i].answer) { this.marks++; }
   }
   document.writeln('your score is ' + this.marks);
 }

 recursivecheck() {
   const result1 = this.quizList;
   const result2 = this.answerkey;

   const props = ['id', 'answer'];

   const result = result1.filter(o1 => {
     // filter out (!) items in result2
     return result2.some(o2 => {
       return o1.answer === o2.answer;
       // assumes unique id
     });

   }).map(o => {
     return props.reduce((newo, ans) => {
       newo[ans] = o[ans];
       return newo;
     }, {});
   });
   console.log('result:' + JSON.stringify(result));
 }

}

export class AnswerKey {
  choosen: string;
  answer: string;
  constructor(choosen: string, answer: string) {
    this.choosen = choosen;
    this.answer = answer;
  }
}
