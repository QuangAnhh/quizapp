export class Quizmodel {

  ID: number;
  identity: string;
  question: string;
  anslistobj: string[];
  answer: string;

  constructor(  ID: number, identity: string, question: string,
                anslistobj: string[], answer: string) {
      this.ID = ID;
      this.identity = identity;
      this.anslistobj = anslistobj;
      this.answer = answer;
 }
}
